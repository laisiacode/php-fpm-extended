FROM php:7.2-fpm

WORKDIR /code

# composer
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

# install lib from apt
RUN apt-get update && \
        apt-get install -y libicu-dev libzip-dev git
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# configure and install php extension
RUN docker-php-ext-configure intl; \
        docker-php-ext-configure zip --with-libzip; \
        docker-php-ext-install intl zip pdo_mysql opcache

